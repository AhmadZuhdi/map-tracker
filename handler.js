const clipboardy = require('clipboardy');
// const notifier = require('node-notifier');
const get = require('lodash.get');
const moment = require("moment");
const uuid = require("uuid");

module.exports = exports.default = class Handler {

	constructor() {
		this.currentMap = {};
		this.currentLoot = [];
		this.mapPool = [];
	}

	handle(event) {
		const {keycode} = event;
		
		if (keycode == 46) {
			this.copyListener();
		}
	}

	copyListener() {
		const text = clipboardy.readSync();

		if (text.includes("--------")) {
			
			const itemInfo = this.dataBuilder(text);

			if (itemInfo.isMap) {
				return this.handleMap(itemInfo);
			} else {
				return this.handleOtherItem(itemInfo);
			}
		}
	}

	handleMap(itemInfo) {
		console.log(itemInfo);

		const mapData = itemInfo;
		const {mapRarity, isUnidentified} = mapData;

		if (mapRarity === 'rare' && !isUnidentified) {
			return {
				type: "new_map",
				data: mapData,
			}
		} else {
			return {
				type: 'loot',
				data: mapData
			}
		}

		return itemInfo;
	}

	handleOtherItem(itemInfo) {
		return {
			type: 'loot',
			data: itemInfo
		}
	}

	dataBuilder(rawText) {

		const splittedText = rawText.split("--------").map(t => t.split("\r\n").filter(n => n))

		const isUnidentified = rawText.includes("Unidentified");
		const isMap = rawText.includes("Map Tier");
		const isCurrency = rawText.includes("Rarity: Currency");
		const isDivCard = rawText.includes("Rarity: Divination Card");
		const itemName = get(splittedText, '0.1', '');

		let specficData = {};

		if (isMap) {
			specficData = this.dataBuilderMap(splittedText, rawText);
		}

		if (isCurrency) {
			specficData = this.dataBuilderCurrency(splittedText, rawText);
		}

		const data = {
			id: uuid.v4(),
			isUnidentified,
			isMap,
			isCurrency,
			isDivCard,
			itemName,
			rawText,
			createdAt: moment()
		};

		return Object.assign({}, data, specficData);
	}

	dataBuilderMap(itemInfo) {
		const mapRarity = get(itemInfo, '0.0', '').toLowerCase().replace(/rarity: /g, '');
		let mapName = get(itemInfo, '0.1', '');

		if (mapRarity !== "normal") {
		    mapName = get(itemInfo, '0.2', '');
        }

		return {
			mapRarity,
			mapName
		}
	}

	
	dataBuilderCurrency(itemInfo, rawText) {
		const currencyStackCount = get(itemInfo, '1.0', '').toLowerCase().replace(/stack size: /g, '').split("/")[0];

		return {
			currencyStackCount
		};
	}
}