// Update with your config settings.

module.exports = {
    client: 'sqlite3',
    useNullAsDefault: true,
    connection: {
        filename: `${__dirname}/poe-tracker.db`
    }
};
