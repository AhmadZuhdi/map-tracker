const {knex} = require("./models/BaseModel");

async function createSchema() {
    // Create database schema. You should use knex migration files to do this. We
    // create it here for simplicity.
    await knex.schema.createTableIfNotExists('Maps', table => {
        table.uuid('id').primary();
        table.string("mapName");
        table.text("rawText");

        table.timestamp('createdAt').defaultTo(knex.fn.now());
        table.timestamp('updatedAt').defaultTo(knex.fn.now());
        table.timestamp('deletedAt').defaultTo(null);
    });

    await knex.schema.createTableIfNotExists('Loots', table => {
        table.uuid('id').primary();
        table.uuid("mapId").references('Maps.id').onDelete('cascade');

        table.integer("currencyStackCount");
        table.string("itemName");
        table.string("mapName");
        table.string("mapRarity");

        table.boolean("isCurrency");
        table.boolean("isDivCard");
        table.boolean("isMap");
        table.boolean("isUnidentified");
        table.text("rawText");

        table.timestamp('createdAt').defaultTo(knex.fn.now());
        table.timestamp('updatedAt').defaultTo(knex.fn.now());
        table.timestamp('deletedAt').defaultTo(null);
    });
};

createSchema();