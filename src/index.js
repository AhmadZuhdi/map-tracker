import React from 'react';
import { render } from 'react-dom';
import App from './components/App';
import { LocaleProvider, DatePicker, message } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';

// Since we are using HtmlWebpackPlugin WITHOUT a template, we should create our own root node in the body element before rendering into it
let root = document.createElement('div');
root.id = "root";
document.body.appendChild( root );

// TODO: update to use import rather than cdn, so it could work offline
// const link = document.createElement('link');
// link.rel = 'stylesheet';
// link.href = 'https://cdnjs.cloudflare.com/ajax/libs/antd/3.4.3/antd.min.css';
// document.head.appendChild(link);

// Now we can render our application into it
render( <LocaleProvider locale={enUS}>
    <App/>
</LocaleProvider>, document.getElementById('root') );
