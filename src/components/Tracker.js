import '../assets/css/App.less';
import React, {Component} from 'react';
import get from 'lodash.get';
import moment from 'moment';
import prettyMS from 'pretty-ms';
import firebase from 'firebase';
import * as uuid from 'uuid';
import {Button, Divider, Avatar, Badge, Tag} from 'antd';

const ws = new WebSocket('ws://localhost:8888');

class App extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            wsConnected: false,
            latestMap: {},
            latestMapStartTime: null,
            timerId: null,
            latestLoots: [],
        }
    }

    render() {
        return (
            <div>
                <h1>PoE Map (+Loot) Tracker!</h1>
                <h2>Status: {this.state.wsConnected ? "Connected" : "Disconnected"}</h2>
                <p>Token : {this.state.token}</p>
                <Divider/>

                {this.state.latestMap.rawText && [
                    <h2>Latest Map:</h2>,
                    <pre style={{width: 500, overflow: "hidden", textOverflow: "ellipsis"}}>{get(this.state, 'latestMap.rawText')}</pre>,
                    <Button onClick={() => this.manualSaveAndReset()}>Save</Button>
                ]}

                {(this.state.latestMap.rawText && !this.state.latestMapStartTime) && (
                    <Button onClick={() => this.startMap()}>Start Track Time</Button>
                )}

                {this.state.latestMapStartTime && (
                    <Button onClick={() => this.finishMap()}>Finish</Button>
                )}

                {this.state.latestMapDiff && prettyMS(this.state.latestMapDiff)}

                {this.state.latestLoots.length > 0 && [
                    <Divider/>,
                    <h2>Loots</h2>,
                    this.renderLoots()
                ]}
            </div>
        );
    }

    removeLoot(key) {
        const {latestLoots} = this.state;

        console.log(latestLoots);
        latestLoots.splice(key, 1);
        console.log(latestLoots);

        this.setState({latestLoots});
    }

    renderLoots() {

        return (
            <div>
                {this.state.latestLoots.map((loot, i) => {
                    let content = {
                        text: loot.itemName
                    };

                    const imageDiv = require("./../assets/images/divCard.png")

                    if (loot.isDivCard) {
                        return (
                            <Badge onClick={() => this.removeLoot(i)} count={"X"}>
                                <Avatar style={{margin: 5}} src={imageDiv} />
                            </Badge>
                        )
                    }

                    const image = require("./../assets/images/" + loot.itemName + ".png")

                    if (loot.isCurrency) {
                        content = this.renderLootCurrency(loot);

                        return (
                            <Badge onClick={() => this.removeLoot(i)} count={loot.currencyStackCount + " (X)"}>
                                <Avatar style={{margin: 5}} src={image} />
                            </Badge>
                        )
                    }

                    return (
                        <Badge onClick={() => this.removeLoot(i)} count={"X"}>
                            <Avatar style={{margin: 5}} src={image} />
                        </Badge>
                    )
                })}   
            </div>
        )
    }

    renderLootMap(loot) {
        return (
            <div>
                {loot.itemName}
            </div>
        )
    }

    renderLootCurrency(loot) {

        const image = require("./../assets/images/" + loot.itemName + ".png")

        console.log(image);

        return {
            text2: `${loot.itemName} ${loot.currencyStackCount}`,
            text: <Avatar src={image} />,
        }
    }

    componentDidMount() {
        ws.addEventListener('open', (event) => {
            this.setState({wsConnected: true});

            ws.addEventListener("message", event => {
                // console.log(event);
                const data = JSON.parse(event.data);
                this.newMessageHandler(data);
            })
        });

        ws.addEventListener('close', event => {

            this.setState({wsConnected: false});
        });

        const token = localStorage.getItem("token");

        if (!token) {
            localStorage.setItem("token", uuid.v4());
        }

        this.setState({
            token: localStorage.getItem("token")
        })
    }

    async newMessageHandler(data) {
        console.log(data);
        if (data.type === 'new_map') {

            if (this.state.latestMap.rawText) {
                await this.saveMapToFB();
            }

            this.setState({
                latestMap: data.data,
                latestMapStartTime: null,
                latestMapDiff: null,
                latestLoots: []
            });

        } else if (data.type === 'loot') {

            const {latestLoots} = this.state;
            latestLoots.push(data.data);
            this.setState({latestLoots});
        }
    }

    saveMapToFB() {
        const db = firebase.database();
        
        return new Promise(resolve => {
        
            ws.send(JSON.stringify({
                map: this.state.latestMap,
                loots: this.state.latestLoots,
                duration: this.state.latestMapDiff,
                map_start: this.state.latestMapStartTime
            }));

            resolve();
        });
    }

    async manualSaveAndReset() {

        await this.saveMapToFB();

        this.setState({
            latestMap: {},
            latestMapStartTime: null,
            latestMapDiff: null,
            latestLoots: []
        });
    }

    createMapTimer() {
        return setInterval(() => {
            const {latestMapStartTime} = this.state;

            if (latestMapStartTime) {
                const latestMapDiff = (new Date()) - latestMapStartTime

                this.setState({latestMapDiff});
            }

        }, 100);
    }

    startMap() {
        const timerId = this.createMapTimer();
        this.setState({
            timerId,
            latestMapStartTime: new Date()
        })
    }

    finishMap() {
        const {timerId} = this.state;
        clearInterval(timerId);

        this.setState({
            latestMapStartTime: null,
            latestMapDiff: (new Date()) - this.state.latestMapStartTime
        });
    }
}

export const Tracker = App;
export default App;
