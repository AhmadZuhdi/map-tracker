import React from 'react';
import { Layout, Tabs } from 'antd';
import {Tracker} from "./Tracker";
const antdCSS = require("antd/dist/antd.min.css");

const { Header, Footer, Sider, Content } = Layout;
const {TabPane} = Tabs;

export default class App extends React.Component {
    render() {

        const style = {
            tabPane: {
                padding: 15
            }
        };

        return (
            <Layout style={{height: '100%', minHeight: "100vh"}}>
                <Content>
                    <Tabs defaultActiveKey="1">
                        <TabPane tab="Tracker" key="1" style={style.tabPane}>
                            <Tracker/>
                        </TabPane>
                        <TabPane tab="Summary" key="2" style={style.tabPane}>

                        </TabPane>
                        <TabPane tab="History" key="3" style={style.tabPane}></TabPane>
                    </Tabs>
                </Content>
            </Layout>
        )
    }
}