const { Model } = require('objection');
const Knex = require('knex');
const config = require("./../knexfile");
const uuid = require("uuid");

const knex = Knex(config);

// Give the knex object to objection.
Model.knex(knex);

module.exports.knex = knex;

module.exports.BaseModel = class BaseModel extends Model {
	$beforeInsert() {
        this.id = this.id || uuid.v4();
        this.createdAt = new Date().toISOString();
    }

    $beforeUpdate() {
        this.updatedAt = new Date().toISOString();
    }

};