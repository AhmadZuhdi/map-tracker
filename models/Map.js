const {BaseModel} = require("./BaseModel");

class MapModel extends BaseModel {
    static get tableName() {

    	return "Maps";
    }
}

module.exports = MapModel;