const isWin = process.platform === "win32";
let ioHook;


if (isWin) {
    ioHook = require('iohook');
} else {
    ioHook = {
        registerShortcut: () => {},
        start: () => {},
    }
}

require("./database");

const MapModel = require("./models/Map");
const LootModel = require("./models/Loot");
const {transaction} = require("objection");
const pick = require("lodash.pick");
const omit = require("lodash.omit");
const uuid = require("uuid");

const Handler = require("./handler");
const handler = new Handler();

const WebSocket = require('ws');

const wsServer = new WebSocket.Server({
    port: 8888,
});

wsServer.on('connection', function connection(ws) {
	console.log("new connection");

	// ctrl + c
    ioHook.registerShortcut([29, 46], keys => {
        const result = handler.copyListener();

        
        try {
            ws.send(JSON.stringify(result))
        } catch (e) {
            console.error(e);
            ws.close();
        }
    });

    ws.on("message", message => {
        const {map={}, loots=[]} = JSON.parse(message);
        
        transaction(MapModel.knex(), async trx => {

            const mapId = uuid.v4();
            const mapData = pick(map, ['mapName', 'rawText']);
            mapData.id = mapId;

            await MapModel.query(trx)
                .insert(mapData);


            return Promise.all(loots.map(loot => {
                const lootData = loot;
                lootData.mapId = mapId;
                return LootModel.query(trx)
                    .insert(lootData)
            }))
        })
        .catch(err => console.error(err));
    });

    ioHook.start();
});