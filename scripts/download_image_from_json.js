const download = require('image-downloader');
const currency = require("./../example_data/currency_image_map.json");
const maps = require("./../example_data/map_image_map.json");

// console.log(currency);

currency.forEach(curr => {
	const options = {
	  	url: curr.src,
	  	dest: __dirname + '/../src/assets/images/' + curr.currencyName + ".png"
	};

	download.image(options)
	  .then(({ filename, image }) => {
	    console.log('File saved to', filename)
	  }).catch((err) => {
	    throw err
	  })
})

maps.forEach(curr => {
	const options = {
	  	url: curr.src,
	  	dest: __dirname + '/../src/assets/images/' + curr.mapName + ".png"
	};

	download.image(options)
	  .then(({ filename, image }) => {
	    console.log('File saved to', filename)
	  }).catch((err) => {
	    throw err
	  })
})