$(".c-item-hoverbox").map((i, elem) => {
	const src = $(elem).find("img").attr("src");
	const currencyName = $(elem).find("a").attr("title");

	return {src, currencyName};
}).get()
